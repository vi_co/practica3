package mx.unitec.practica3

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_picker.*
import kotlinx.android.synthetic.main.activity_picker_date.*
import mx.unitec.practica3.ui.DatePickerFragment
import mx.unitec.practica3.ui.TimePickerFragment

class PickerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)
    }

    fun showDatePickerDialog(v: View) {
        //val timerPickerFragment = TimePickerFragment()

        val DatePickerFragment = DatePickerFragment.newInstance(DatePickerDialog.OnDateSetListener
        { view, year, month, dayOfMonth ->
            pkrDate.setText("${year}:${month}:${dayOfMonth}")

        })

        /*val DatePickerFragment = DatePickerFragment.newInstance(DatePickerDialog.OnDateSetListener
        { view, year, month, day ->
            pkrDate.setDate*/

            //pkrTime.setText("${year}:${month}:${day}")


        DatePickerFragment.show(supportFragmentManager, "datePicker")

    }

}