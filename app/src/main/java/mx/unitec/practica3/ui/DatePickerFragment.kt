package mx.unitec.practica3.ui

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.text.format.DateFormat
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import java.time.Year
import java.util.*

class DatePickerFragment: DialogFragment (), {

    private var listener : DatePickerDialog.OnDateSetListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val calendar = Calendar.getInstance()
        val year = calendar.get(calendar. Year )
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(calendar. firstDayOfWeek )

        return DatePickerDialog(this, listener, year, month, day )
    }
    fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
    Toast.makeText(activity, "${day}:${year}:${month}",Toast.LENGTH_LONG).show()
    }

    companion object {

        fun newInstance(listener: DatePickerDialog.OnDateSetListener) : DatePickerFragment{
            val fragment = DatePickerFragment()
            fragment.listener = listener
            return fragment
        }
    }



}