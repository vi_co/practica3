package mx.unitec.practica3.ui

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.format.DateFormat
import androidx.fragment.app.DialogFragment
import java.util.*

class TimePickerFragment: DialogFragment() {

    private var listener : TimePickerDialog.OnTimeSetListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val calendar = Calendar.getInstance()
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)


        return TimePickerDialog(activity, listener, hour, minute, DateFormat.is24HourFormat(activity))
    }
    companion object{
        fun newInstance(listener: DatePickerDialog.OnDateSetListener) : TimePickerFragment {
            val Fragment = TimePickerFragment()
            Fragment.listener = listener
            return Fragment

        }


    }

    //override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
    //  Toast.makeText(activity, "${hourOfDay}:${minute}", Toast.LENGTH_SHORT).show()
    //}



}