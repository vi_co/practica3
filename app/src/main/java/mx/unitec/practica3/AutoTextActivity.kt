package mx.unitec.practica3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_auto_text.*

class AutoTextActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auto_text)

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.alcaldia_array,
            android.R.layout.select_dialog_item

        )

        autoCompleteTextViewAlcaldía.threshold = 2

        autoCompleteTextViewAlcaldía.setAdapter(adapter)

        autoCompleteTextViewAlcaldía.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText( this,
            position.toString() + ":" + parent?.getItemAtPosition(position).toString(),
            Toast.LENGTH_LONG).show()
        }

    }
}