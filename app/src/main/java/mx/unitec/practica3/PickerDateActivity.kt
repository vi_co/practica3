package mx.unitec.practica3

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_picker_date.*
import mx.unitec.practica3.ui.TimePickerFragment

class PickerDateActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker_date)
    }

    fun showDatePickerDialog(v: View){
        //val DatePickerFragment = DatePickerFragment()
        val DatePickerFragment = TimePickerFragment.newInstance(DatePickerDialog.OnDateSetListener
        { view, year, month, dayOfMonth ->
            pkrDate.setText("${year}:${month}:${dayOfMonth}")
        })
        DatePickerFragment.show(supportFragmentManager, "datePicker" )

    }


}